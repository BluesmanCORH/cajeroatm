package com.bank;

import com.bank.log.Log;
import com.bank.account.Client;
import com.bank.account.auth.Auth;
import com.bank.account.exceptions.*;

import java.util.Scanner;

public class Main {



    public static void main(String[] args) {
	// write your code here

        System.out.println("Starting test...");
        Scanner sc = new Scanner(System.in);
        Auth auth = new Auth();
        Client cliente = null;
        String in = null;
        Log log;


        do {
            String logMessage = "Authenticated successfully";

            System.out.println("Enter your card number or enter EXIT to finish");
            in = sc.nextLine();
            if (in.toUpperCase().trim().equals("EXIT"))
                continue;
            String numero_tarjeta = in;
            log = new Log(numero_tarjeta);

            System.out.println("Enter your email or your NIP. Enter EXIT to finish");
            in = sc.nextLine();
            if (in.toUpperCase().trim().equals("EXIT"))
                continue;
            String pass = in;

            // El método devuelve un objeto con los datos del cliente, en caso de que exista

            try {
                cliente = auth.isValid(numero_tarjeta, pass);
                logMessage += " "+cliente.getName()+"|"+cliente.getAccountNumber();
            } catch (IncorrectDataLengthException e) {
                logMessage = "The NIP does not have the allowed length";
                System.out.println(logMessage);

                continue;
            } catch (EmptyEmailException e) {
                logMessage = "The email is empty";
                continue;
            } catch (InvalidEmailFormatException e) {
                logMessage = "The email does not have the correct format";
                continue;
            } catch (EmptyCardNumberException e) {
                logMessage = "The card number is empty";
                continue;
            } catch (EmptyNipException e) {
                logMessage = "The NIP is empty";
                continue;
            } catch (InvalidDataTypeException e) {
                logMessage = "The card number is not numeric";
                continue;
            } catch (InvalidAuthException e) {
                logMessage = "The user does not exist or the validation was not successful";
                continue;
            } catch (DataAssignmentException e) {
                logMessage = "A parameter assignment error has occurred";
                continue;
            } catch (IncorrectNumberCardLengthException e) {
                logMessage = "The card number has not the allowed length";
                continue;
            }finally {
                System.out.println(logMessage);
                log.save(logMessage);
                logMessage = "";
            }

            break;

        } while(in != null && !in.equals("EXIT"));

        MainMenu menu = new MainMenu(cliente);
        menu.showMenu();

    }

}
