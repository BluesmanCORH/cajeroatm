package com.bank;

import com.bank.data.Bank;
import com.bank.account.Client;
import com.bank.enums.OptionsMainMenu;

import java.util.Arrays;
import java.util.Scanner;

public class MainMenu {

    private Bank bank;
    private OptionsMainMenu options;

    MainMenu(Client client){
        this.bank = new Bank(client);
    }

    public void showMenu(){
        int selection;
        Scanner entrada = new Scanner(System.in);

        final String[] MAIN_MENU_OPTIONS  = {"Select an Option: ","    1.Check Balance","    2.Pay Services","    3.Withdraw Money", "    4.Exit"};
        try{
            do{
                Arrays.stream(MAIN_MENU_OPTIONS).forEach(System.out::println);
                selection = entrada.nextInt();
                options = options.values()[selection - 1];
                optionSelector(options);
            }while(options != OptionsMainMenu.EXIT);
        }catch(Exception e){
            System.err.println("Error, Choose a correct option \n");
            showMenu();

        }

    }

    private void optionSelector(OptionsMainMenu opt){

        switch (opt){
            case GET_BALANCE:
                bank.getBalance();
                break;
            case PAY_SERVICES:
                bank.payServices();
                break;
            case WITHDRAW_MONEY:
                bank.withdraw();
                break;
            case EXIT:
                System.exit(0);
                break;
            default:

        }
    }
}
