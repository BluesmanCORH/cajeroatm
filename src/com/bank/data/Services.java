package com.bank.data;

public interface Services {

    public abstract void getBalance();
    public abstract void payServices();
    public abstract double withdraw();
}
