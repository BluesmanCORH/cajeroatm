package com.bank.data;

import com.bank.log.Log;


import com.bank.enums.Options;


import java.util.Arrays;
import java.util.Scanner;


import com.bank.account.Client;

public class Bank implements Services {

    private Client client;

    private Log log;
    private final String GREEN = "\033[0;36m";
    private final String DEFAULT = "\u001B[0m";
    private final String WHITE_UNDERLINED = "\033[4;37m";


    public Bank(Client client) {
        this.client = client;
        this.log = new Log(String.valueOf(client.getAccountNumber()));
    }

    @Override
    public void getBalance() {
        System.out.println("************************************************");
        System.out.println("your current balance is: $"+ WHITE_UNDERLINED +client.getBalance() + DEFAULT);
        System.out.println("************************************************");
    }

    @Override
    public void payServices() {
        printMenu();

    }

    @Override
    public double withdraw() {
        Double amount = askAmount("Enter the amount to withdraw");
        if(amount > 0){
            checkPayment(amount,"The transaction was successful for the amount of $"+amount );
        }
        return amount;

    }


    private void payService(Options opc){
        String[] companies = {"Select company name: ","    1. Telcel","    2. AT&T","    3. Movistar", "    4. Exit"};
        Scanner input = new Scanner(System.in);

        int selection = 0;
        boolean flag = false;

        double amount = 0d;

        do{
        switch (opc){
            case CELL_MINUTES:
                try{

                Arrays.stream(companies).forEach(System.out::println);
                selection = input.nextInt();

                if(selection >=4 || selection <=0) flag = true;
                else{
                    String company = companies[selection ].split(" ")[1];
                    amount = askAmount("Enter the amount: ");
                    String message = "You have made the payment to the bank " + company +" the amount of $" + amount+"" +
                            " for this number "+client.getPhone();
                    flag = true;
                    checkPayment(amount,message);
                }
                }catch(Exception e){
                    System.out.println("Enter a correct value");
                    input.nextLine();
                }

                break;

            case NETFLIX:
                amount = askAmount("Enter the amount: ");
                if(amount > 0){
                    String message = "You have made the payment to Netflix for the amount of $" + amount+ " for this account " +
                            client.getEmail();
                    flag = true;
                    checkPayment(amount,message);
                }

                break;

            case TRANSFER:

                long accountNumber = 0;
                try{

                    System.out.println("Enter the account number");
                    accountNumber = input.nextLong();
                    amount = askAmount("Enter the amount to transfer:");
                    if(amount > 0){
                        String message = "You have successfully made the transfer for the amount of $" + amount + " to the account number " + accountNumber ;
                        flag = true;
                        checkPayment(amount,message);
                    }

                }catch(Exception e){
                    System.err.println("Enter a correct value");
                    input.nextLine();
                    flag = true;


                }


                break;

            case LOGOUT:
                flag = true;
                break;

        }
        }while(!flag);

    }

    private void checkPayment(double amount, String message){
        if(this.client.getBalance() - amount >= 0){

            this.client.setBalance(this.client.getBalance()- amount);
            System.out.println(GREEN + message + DEFAULT);

            System.out.println("Your current balance is: $" +  WHITE_UNDERLINED + this.client.getBalance() + DEFAULT + "\n");
            saveLog(message);
        }else{
            System.err.println("It is not possible to make the requested movement for the amount of $" + amount + "\n");

        }
    }
    private void printMenu(){
        int selection;
        Scanner input = new Scanner(System.in);
        Options opc;
        String[] optionsList = {"Select an Option:","    1.Cell Minutes","    2.Netflix","    3.Transfer","    4.logout"};
        try{
            do{
                Arrays.stream(optionsList).forEach(System.out::println);
                selection = input.nextInt();
                opc = Options.values()[selection - 1];
                this.payService(opc);
            }while(opc != Options.LOGOUT);
        }catch(Exception e){
            System.err.println("Error, Choose a correct option \n");
            printMenu();

        }

    }


    private Double askAmount(String message){

        Scanner input = new Scanner(System.in);
        try{

            System.out.println(message);
            double amount = input.nextDouble();
            return amount;
        }catch(Exception e){
            System.err.println("Enter a correct value");
            input.nextLine();
            return -1D;
        }
    }

    private void saveLog(String message){
        log.save(message);
    }
}


