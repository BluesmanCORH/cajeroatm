package com.bank.account.exceptions;

public class IncorrectDataLengthException extends Exception {
    public IncorrectDataLengthException() {
        super();
    }
}
