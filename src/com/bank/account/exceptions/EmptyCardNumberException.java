package com.bank.account.exceptions;

public class EmptyCardNumberException extends Exception {
    public EmptyCardNumberException() {
        super();
    }
}
