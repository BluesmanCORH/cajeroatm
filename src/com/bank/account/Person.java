package com.bank.account;

public class Person {
	protected String name;

	protected String email;

	protected long Phone;

	public Person(String name, String email, long phone) {
		this.name = name;
		this.email = email;
		this.Phone = phone;
	}

	public String getName() {

		return name;
	}

	public String getEmail() {

		return email;
	}

	public long getPhone() {

		return Phone;
	}
}
