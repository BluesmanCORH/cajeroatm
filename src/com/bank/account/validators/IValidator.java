package com.bank.account.validators;

public interface IValidator {
    boolean validate();
}
