package com.bank.account.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsEmail implements IValidator {
    private String in;

    public IsEmail(String in) {
        this.in = in;
    }

    @Override
    public boolean validate() {
        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(this.in);

        if (!mather.find()) {
            return false;
        }
        else {
            return true;
        }
    }
}
