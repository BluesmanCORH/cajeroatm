package com.bank.account.validators;

import java.util.regex.Pattern;

public class IsNumber implements IValidator{

    private String in;

    public IsNumber(String in) {
        this.in = in;
    }

    @Override
    public boolean validate() {
        if (!Pattern.matches("[0-9]*", this.in)) {
            return false;
        }
        return true;
    }
}
