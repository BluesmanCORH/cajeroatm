package com.bank.account.validators;

public class HasLength implements IValidator {

    private String in;

    private int length;

    public HasLength(String in, int length) {
        this.in = in;
        this.length = length;
    }

    @Override
    public boolean validate() {
        if (this.in.length() != this.length) {
            return false;
        }
        return true;
    }
}
