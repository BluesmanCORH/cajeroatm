package com.bank.account.validators;

public class Validator {

    public static boolean val(IValidator ... valids) {
        for (IValidator val : valids) {
            if (!val.validate())
                return false;
        }
        return true;
    }

}
