package com.bank.account.validators;

public class IsEmpty implements IValidator {
    private String in;

    public IsEmpty(String in)
    {
        this.in = in;
    }

    @Override
    public boolean validate() {
        if (this.in == null) {
            return true;
        }
        else if (this.in.equals("")) {
            return true;
        }
        else {
            return false;
        }
    }
}
