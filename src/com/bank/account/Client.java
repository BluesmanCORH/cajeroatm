package com.bank.account;

import java.util.regex.Pattern;

public class Client extends Person {
	
	private long accountNumber;

	private long accountCard;

	private double balance;

	private int nip;

	public Client(String name, String email, long phone, long accountNumber, long accountCard, double balance, int nip) {
		super(name, email, phone);
		this.accountNumber = accountNumber;
		this.accountCard = accountCard;
		this.balance = balance;
		this.nip = nip;
	}
	
	public long getAccountNumber() {
		return accountNumber;
	}

	public long getAccountCard() {
		return accountCard;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getNip() {
		return this.nip;
	}

	@Override
	public String toString() {
		return
			name + "|" +
			email + "|" +
			String.valueOf(getPhone()) + "|" +
			String.valueOf(accountNumber) + "|" +
			String.valueOf(accountCard) + "|" +
			String.valueOf(balance) + "|" +
			String.valueOf(nip);
	}

	public static Client toClient(String in) {
		String[] datos = in.split(Pattern.quote("|"));

		String nombre = datos[0];
		String email = datos[1];
		String phone = datos[2];
		String accountNumber = datos[3];
		String accountCard = datos[4];
		String balance = datos[5];
		String nip = datos[6];

		return new Client(
				nombre,
				email,
				Long.valueOf(phone),
				Long.valueOf(accountNumber),
				Long.valueOf(accountCard),
				Double.valueOf(balance),
				Integer.valueOf(nip)
		);
	}

}
