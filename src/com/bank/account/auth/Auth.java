package com.bank.account.auth;

import com.bank.account.Client;
import com.bank.account.exceptions.*;
import com.bank.account.persistence.interfaces.IAuthPersistence;
import com.bank.account.persistence.types.TxtFilePersistence;
import com.bank.account.types.CardNumberWrap;
import com.bank.account.types.EmailWrap;
import com.bank.account.types.NipWrap;

public class Auth {

    // Valida a través del número de tarjeta (16 dígitos), el correo electrónico y/o el nip (4 dígitos)
    public Client isValid(String numero_tarjeta, String pass) throws DataAssignmentException, EmptyCardNumberException, IncorrectDataLengthException, InvalidDataTypeException, EmptyEmailException, InvalidEmailFormatException, EmptyNipException, InvalidAuthException, IncorrectNumberCardLengthException {

        // Validando el número de tarjeta
        // En caso de no tener un formato válido, arroja las excepciones correspondientes
        CardNumberWrap cnw = new CardNumberWrap();
        cnw.set(numero_tarjeta);


        if (pass.contains("@")) {
            // Hasta este punto, probablemente sea un email debido a que contiene uno o más @
            // El método isEmail verifica si tiene el formato correcto
            // En caso de que no, arroja las excepciones correspondientes
            isEmail(pass);

            Client cliente = null;
            IAuthPersistence auth_access = new TxtFilePersistence();
            EmailWrap ew = new EmailWrap();
            ew.set(pass);
            cliente = auth_access.searchByEmail(cnw, ew);
            if (cliente != null)
                return cliente;
            else
                throw new InvalidAuthException();
        }
        else if (isNip(pass)){
            // Es un nip
            Client cliente  = null;
            IAuthPersistence auth_access = new TxtFilePersistence();
            NipWrap nw = new NipWrap();
            nw.set(pass);
            cliente = auth_access.searchByNip(cnw, nw);
            if (cliente != null)
                return cliente;
            else
                throw new InvalidAuthException();
        }
        else {
            throw new DataAssignmentException();
        }
    }

    private boolean isEmail(String pass) throws EmptyEmailException, InvalidEmailFormatException {
        EmailWrap ew = new EmailWrap();
        ew.set(pass);
        return true;
    }

    private boolean isNip(String pass) throws IncorrectDataLengthException, InvalidDataTypeException, EmptyNipException {
        NipWrap nw = new NipWrap();
        nw.set(pass);
        return true;
    }
}
