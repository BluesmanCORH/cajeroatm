package com.bank.account.persistence.interfaces;

import com.bank.account.Client;
import com.bank.account.types.CardNumberWrap;
import com.bank.account.types.EmailWrap;
import com.bank.account.types.NipWrap;

public interface IAuthPersistence {

    Client searchByEmail(CardNumberWrap cnw, EmailWrap ew);

    Client searchByNip(CardNumberWrap cnw, NipWrap nw);
}
