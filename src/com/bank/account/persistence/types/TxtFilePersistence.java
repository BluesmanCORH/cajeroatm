package com.bank.account.persistence.types;

import com.bank.account.Client;
import com.bank.account.persistence.interfaces.IAuthPersistence;
import com.bank.account.types.CardNumberWrap;
import com.bank.account.types.EmailWrap;
import com.bank.account.types.NipWrap;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static java.lang.System.out;

public class TxtFilePersistence implements IAuthPersistence {
    private final String DATACLIENTFILENAME = "clients.txt";

    @Override
    public Client searchByEmail(CardNumberWrap cnw, EmailWrap ew) {
        List<Client> clients = this.getData();
        clients = clients
                .stream()
                .filter((Client c) -> c.getAccountCard() == cnw.get().longValue() && c.getEmail().equals(ew.get()))
                .collect(Collectors.toList());

        if (clients == null)
            return null;
        else if (clients.size() <= 0)
            return null;
        else
            return clients.get(0);
    }

    @Override
    public Client searchByNip(CardNumberWrap cnw, NipWrap nw) {
        List<Client> clients = this.getData();
        clients = clients
                .stream()
                .filter((Client c) -> c.getAccountCard() == cnw.get().longValue() && c.getNip() == nw.get().intValue())
                .collect(Collectors.toList());

        if (clients == null)
            return null;
        else if (clients.size() <= 0)
            return null;
        else
            return clients.get(0);
    }

    private List<Client> getData() {
        List<String> data = this.read();
        List<Client> clients = data.stream().map(Client::toClient).collect(Collectors.toList());
        return clients;
    }

    private List<String> read() {
        List<String> list = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(new File(DATACLIENTFILENAME)))) {
            String line;
            while((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            out.println(e.getMessage());
        }

        return list;
    }

    private void write(List<String> list) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(DATACLIENTFILENAME))) {
            for (String l : list) {
                pw.println(l);
            }
        } catch (IOException e) {
            e.printStackTrace();
            out.println(e.getMessage());
        }
    }
}