package com.bank.account.types;

import com.bank.account.exceptions.*;
import com.bank.account.validators.HasLength;
import com.bank.account.validators.IsEmpty;
import com.bank.account.validators.IsNumber;
import com.bank.account.validators.Validator;

public class NipWrap implements ITypeWrap<Integer> {

    private int in;

    @Override
    public void set(String in) throws EmptyNipException, IncorrectDataLengthException, InvalidDataTypeException {
        if (Validator.val(new IsEmpty(in))) {
            throw new EmptyNipException();
        }
        else if (!Validator.val(new HasLength(in, 4))) {
            throw new IncorrectDataLengthException();
        }
        else if (!Validator.val(new IsNumber(in))) {
            throw new InvalidDataTypeException();
        }else {
            this.in = Integer.valueOf(in);
        }
    }

    @Override
    public Integer get() {
        return this.in;
    }
}