package com.bank.account.types;

import com.bank.account.validators.IsEmail;
import com.bank.account.validators.IsEmpty;
import com.bank.account.validators.Validator;
import com.bank.account.exceptions.*;

public class EmailWrap implements  ITypeWrap<String> {
    private String in;

    @Override
    public void set(String in) throws EmptyEmailException, InvalidEmailFormatException {
        if (Validator.val(new IsEmpty(in))) {
            throw new EmptyEmailException();
        }
        else if (!Validator.val(new IsEmail(in))) {
            throw new InvalidEmailFormatException();
        }
        else
        {
            this.in = in;
        }
    }

    @Override
    public String get() {
        return this.in;
    }
}
