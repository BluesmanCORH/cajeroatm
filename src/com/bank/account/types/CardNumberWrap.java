package com.bank.account.types;

import com.bank.account.exceptions.IncorrectNumberCardLengthException;
import com.bank.account.validators.HasLength;
import com.bank.account.validators.IsEmpty;
import com.bank.account.validators.IsNumber;
import com.bank.account.validators.Validator;
import com.bank.account.exceptions.EmptyCardNumberException;
import com.bank.account.exceptions.InvalidDataTypeException;

public class CardNumberWrap implements ITypeWrap<Long> {
    private long card_number;

    @Override
    public void set(String in) throws EmptyCardNumberException, InvalidDataTypeException, IncorrectNumberCardLengthException {
        if (Validator.val(new IsEmpty(in))) {
            // El número de tarjeta se encuentra vacío
            throw new EmptyCardNumberException();
        }
        else if (!Validator.val(new HasLength(in, 16))) {
            // El número de tarjeta excede los 16 dígitos
            throw new IncorrectNumberCardLengthException();
        }
        else if (!Validator.val(new IsNumber(in))) {
            // El tipo de dato de la entrada no es el correcto
            throw new InvalidDataTypeException();
        } else
        {
            this.card_number = Long.valueOf(in);
        }
    }

    @Override
    public Long get() {
        return this.card_number;
    }
}
