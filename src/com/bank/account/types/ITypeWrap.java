package com.bank.account.types;

import com.bank.account.exceptions.*;

public interface ITypeWrap<T> {

    void set(String in) throws DataAssignmentException, EmptyCardNumberException, IncorrectDataLengthException, InvalidDataTypeException, EmptyEmailException, InvalidEmailFormatException, EmptyNipException, IncorrectNumberCardLengthException;

    T get();

}
