package com.bank.log;

import java.io.FileWriter;
import java.io.PrintWriter;

public class FileWriterer {

    private FileWriter fileWriter;
    private PrintWriter printWriter;
    private String fileName;

    FileWriterer(String fileName){
        this.fileName = fileName;
    }

    public void write(String message){

        try (FileWriter fileWriter2 = fileWriter = new FileWriter(fileName, true)) {
            printWriter = new PrintWriter(fileWriter2);
            printWriter.append("\n"+message);
            fileWriter.close();
        }catch(Exception ex) {
            System.err.println("Error on log file!");
        }
    }


}
