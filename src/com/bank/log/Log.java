package com.bank.log;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Log {

    private String accountNumber;
    private LocalDateTime dateTime;
    private DateTimeFormatter formatter;
    private final String FILENAME =  "./log.txt";
    private FileWriterer fileWriterer;

    public Log(String accountNumber) {
        this.accountNumber = accountNumber;

        fileWriterer = new FileWriterer(FILENAME);
        formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    }




    public void save(String operation){
        StringBuilder builder = new StringBuilder();
        dateTime = LocalDateTime.now();

        builder.append(dateTime.format(formatter));
        builder.append("|");
        builder.append(accountNumber);
        builder.append("|");
        builder.append(operation);
        fileWriterer.write(builder.toString());
    }
}
